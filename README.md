# Debian Bullseye



## Getting started

- Entrar a la página de Virtualbox.org y dirigirse al dashboard para empezar la descarga del Virtualbox. Para dirigirse a la página, dale click al siguiente link: https://www.virtualbox.org/wiki/Downloads
- Una vez descargado el Virtualbox se puede proceder a la instalación del mismo, al iniciar el archivo ejecutable se abrirá una ventana emergente con las instrucciones para instalar el Virtualbox, allí podrás configurar la ruta de instalación, el idioma, entre otras.
![Virtualbox](/uploads/ff31b34c9da859e861c6105c5c7613d4/Virtualbox.jpg)

- Entrar a la página de Vagrant, en la ventana principal aparecerá un botón de descarga con la versión más reciente, darle click y luego elegir el sistema operativo deseado (Vagrant reconoce el sistema operativo de la máquina).
- Una vez descargado el Vagrant se puede proceder a la instalación del mismo, al iniciar el archivo ejecutable se abrirá una ventana emergente con las instrucciones para instalar el Vagrant, allí podrás configurar la ruta de instalación, el idioma, entre otras. 
![Vagrant](/uploads/53112ede8a3414bfc34e8bb7a084f04c/Vagrant.jpg) 

![Vagrant_setup](/uploads/6fadb956e5e58c221c90d0886c0e0f31/Vagrant_setup.jpg)
 
 - Como siguiente paso debemos entrar al directorio de la carpeta en la cual se localiza vagrant y lanzar el comando vagrant init debian/bullseye64 para inicializar y crear la máquina

 - Luego debemos configurar el archivo Vagrantfile descomentado la línea 35, la cual deberia salir asi #config.vm.network "private_network", ip: "192.168.33.10" y luego guardamos y lanzamos el comando vagrant up

 ![img15](/uploads/4bb91a3ce38fcdb736fa5f71251c60f7/img15.jpeg)

 ![img16](/uploads/855352d0b3db8c730ce26b5b08842eb6/img16.jpeg)
 
![img14](/uploads/ba62722f8f6785e69ede71da01946d74/img14.jpeg)

- Luego procedemos a entrar al directorio de la máquina vagrant y cambiamos algunos dominios para la facilidad del taller

![img10](/uploads/6c1b696a43d896d04a647c4e5168d6ea/img10.jpeg)

![img9](/uploads/a529eef695b1a553158f6576e04068d9/img9.jpeg)

**Instalación de los programas para servidor web (Apache2, PHP, MySQL y librerías)**
- En esta parte debemos crear un nuevo usuario con el comando sudo adduser (nombre que se le desee dar al usuario), luego se debe agregar a este usuario al grupo de sudoers para que tenga privilegios y de esa forma ser capaz de cambiar algunos archivos que solo los usuarios de grupo sudoers pueden editar, para agregar al usuario al grupo de sudoers utilizamos el comando sudo gpasswd -a laravel sudo, luego procedemos a actualizar los paquetes con el comando sudo apt-get update, una vez actualizados los paquetes procedemos a instalar los programas para servidor web y realizamos el comando sudo apt-get install (y aquí van los paquetes que necesitamos, como se mostrará en las siguientes imagenes)

![img8](/uploads/108107b673d5626634c22b8ffd536f10/img8.jpeg)

![img7](/uploads/daeea49259b560f12c048c541f0e5554/img7.jpeg)

![img5](/uploads/1c22e1b057ae9aef28be6a1b521747c9/img5.jpeg)

**Prueba de sitio del servidor**
- Una vez terminado de instalar todos los paquetes necesarios del servidor configuramos la dirección en los hosts del web server y le adjuntamos la ip correspondiente que fue la misma que descomentamos en el archivo Vagrantfile en los pasos anteriores y le damos el nombre que queramos y luego nos vamos al navegador para escribir el nombre del dominio que le acabamos de dar

![img4](/uploads/4ffef648b0c33a08472aff8a540a52ca/img4.jpeg)

**Creación de base de datos para Laravel**
Con el comando sudo mysql entramos a la base de datos mariaDB, una vez dentro creamos la base de datos y luego un usuario y le damos  los roles correspondientes, una vez finalizado nos vamos a los sitios de laravel  a configurar en el archivo .env

![mariadb](/uploads/3f68e5c703bedaddbb5591bbd2d3c653/mariadb.jpg)

**Creación de dos sitios con Laravel**

Para crear los sitios laravel primero tenemos que instalar composer que es un paquete necesario para la creación de dichas máquinas, para eso ingresamos a la pagina y realizamos la descarga y sus respectivas configuraciones, luego de tener instalado composer daremos inicio con la creacion de los archivos laravel utilizando los siguientes comandos:
-composer global require laravel/installer
-laravel new laravel.isw811.xyz
Y hacemos lo mismo con el otro archivo:
-composer global require laravel/installer
-laravel new blog.isw811.xyz

- De esta forma tendriamos nuestros dos sitios creados con laravel funcionando de una manera correcta

![img3](/uploads/cf2315bae0b51345d034be6d4f7e4072/img3.jpeg)

![img2](/uploads/3057561b30e0ad6b5cb2ffb4c47c4bc1/img2.jpeg)

![img1](/uploads/df38fac326b12156050b27b19a54b3e8/img1.jpeg)


**Configuración de VHosts para hospedar varios sitios**
- Dentro de la maquina tenemos que crear dos archivos con el comando touch laravel.isw811.xyz.conf y blog.isw811.xyz.conf,
luego de creados pasamos a editarlos con la información necesaria y correcta para un virtual host, luego de editados procedemos a moverlos con el comando sudo mv *.conf /rtc/apache2/sites-avaible/ y esto nos hará que los archivos queden disponibles para nuestro uso y luego procedemos activarlos con los módulos del comando sudo a2enmod vhost_alias rewrite ssl y luego reiniciamos apache con systemctl restart.

![vhost](/uploads/6c40d31e230f5293e5e93ff134d0bacb/vhost.jpg)

![vhost2](/uploads/712e360190dbb425c6356267491e3dfc/vhost2.jpg)

**Configuración de archivos hosts para simular la resolución del dominio**
- Luego configuramos los virtual host de nuestra maquina anfitrión con la IP y los nombres de los sitios para que todo quede listo.

![pagina1](/uploads/d696305cc257e90e9e0b94ca536b64b3/pagina1.jpg)

![pagina2](/uploads/79008f10851741c14cf3e3421af7daa3/pagina2.jpg)
